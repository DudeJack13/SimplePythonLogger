#12/07/2018 v1.2
#+ Added quit key

#12/07/2018 v1.1
#+ Added colours

#09/07/2018 v1.0
#Initial Release

import sys, os, time, win32gui, keyboard ,color_console as cons

#Set up window
hwnd = win32gui.GetForegroundWindow()
win32gui.MoveWindow(hwnd, 0, 0, 500, 800, True)

#Set up log file
fileName = "logFile.txt"
fileLineCount = 1
quitLoggerKey = "Quit Logger"

pathname = os.path.dirname(sys.argv[0]) +  "\\" + fileName
logFile = open(pathname, "w")
logFile.write("Python Live Logger Script: v1.2\n\n")
logFile.close()

default_fg = cons.get_text_attr()
info_fg = cons.FOREGROUND_GREEN
warning_fg = cons.FOREGROUND_YELLOW
error_fg = cons.FOREGROUND_RED

#Print set up on console
logFile = open(pathname, "r")
fileText = logFile.readline()
logFile.close()
print (fileText)
#Get modifcation time
modDate = os.stat(pathname)
checkModDate = modDate

#Keep checking to see if file has been modified
while (True):
	checkModDate = os.stat(pathname)
	if (checkModDate != modDate):
		logFile = open(pathname, "r")
		#Read
		counter = 0
		for line in logFile:
			if quitLoggerKey in line:
				break
				print ("Quitting Logger")
			elif (counter > fileLineCount):
				if (line.split(':', 1)[0] == "INFO"):
					cons.set_text_attr(info_fg)
					print (line.split(':', 1)[0] + ": ")
				elif (line.split(':', 1)[0] == "WARNING"):
					cons.set_text_attr(warning_fg)
					print (line.split(':', 1)[0] + ": ")
				elif (line.split(':', 1)[0] == "ERROR"):
					cons.set_text_attr(error_fg)
					print (line.split(':', 1)[0] + ": ")
				cons.set_text_attr(default_fg)
				print (line.split(': ', 1)[1])
				fileLineCount += 1
			counter += 1
		logFile.close()
		modDate = os.stat(pathname)
	if keyboard.is_pressed('escape'):
		break
#Delete file
os.remove (pathname)
exit()